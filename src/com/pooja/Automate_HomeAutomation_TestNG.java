package com.pooja;


import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Automate_HomeAutomation_TestNG {
	
	WebDriver driver;
	Actions act;
	
	@BeforeClass
       public void setup(){
                 System.setProperty("webdriver.gecko.driver", "E:\\Selenium\\Jar files\\geckodriver-v0.24.0-win32\\geckodriver.exe");
		  
				 driver = new FirefoxDriver();
				 driver.manage().window().maximize();
				 driver.get("http://localhost:8080/HomeAutomation/");
//			 driver.navigate().forward();
//			 driver.q
				 
				 act = new Actions(driver);
	}
	
     @Test()
     public void a_testhomeautomation() throws InterruptedException {
	 Thread.sleep(5000);
	 
	 WebElement first_name = driver.findElement(By.id("element_1_1"));
	 act.moveToElement(first_name).click();
	 act.sendKeys("pooja");
	 act.build().perform();
	 WebElement last_name = driver.findElement(By.id("element_1_2"));
	 act.moveToElement(last_name).click();
	 act.sendKeys("Phatak");
	 act.build().perform();
	 //Thread.sleep(5000);
	
	 
	}
    
     @Test()
     public void b_testhomeautomation()
     {
    	 try {
    		 WebElement dd = driver.findElement(By.id("element_9"));
    		 //WebElement dd = driver.findElement(By.xpath("//form[@id='form_46558']/ul/li[6]/div/select"));
    		 Select sel= new Select(dd);
        	 List<WebElement> all_values = sel.getOptions();
        	 
        	 
//        	// method 1
//        	 System.out.println(all_values.size());
//        	 for(int i=0;i<all_values.size();i++)
//        	 {
//        		 System.out.println(" " +all_values.get(i).getText());
//        	 }
        	 
        	 
//        	 //method 2 - using advance for loop
        	 
        	 for(WebElement all_values1 : all_values)
        		    // for every iteration it will take element at each location(0to5) of array AR1 and assign 
        			//it to object AR
        			{
        			
        				System.out.println(all_values1.getText());
        			}

        	 //method 3 - using iterator
//        	 Iterator<WebElement> itr = all_values.iterator();
//        	 while(itr.hasNext())
//        	 {
//        		 System.out.println(itr.next().getText());
//        	 }
        	 
        	 
       // 	 act.moveToElement(dd).click();
//        	 sel.selectByValue("3");
//        	 sel.selectByIndex(index);
//        	 sel.selectByVisibleText(arg0);
        	 
        	 //driver.switchTo().
//        	
        	 
        	 
        	 
        	 
        	 
         //	 act.build().perform();
        	 Thread.sleep(5000);
        	 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Please enter correct xpath");
		} 
     }
     
     @Test()
     
     public void c_save_form(){
    	 
    	 try {
    		 WebElement submit = driver.findElement(By.id("savebtn"));
    				 submit.click();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Please enter correct xpath");
		}
     }
	
     @Test()
     
     public void d_capture_alert(){
    	 
    	 try {
    	 WebDriverWait wait = new WebDriverWait(driver, 2);
     	 wait.until(ExpectedConditions.alertIsPresent());
     	 Alert alert = driver.switchTo().alert();
     	 String alert_message = alert.getText();
     	 System.out.println("Alert message is" +alert_message );
     	 //Assert.assertEquals(alert_message, "Are you sure you want to save data?");
     	 //System.out.println("Alert message is "+alert_message);
     	 Thread.sleep(5000);
     	 alert.accept();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
     }
     
    
     
     @Test()
     public void e_link_test(){
    	 String subWindowHandler = null;
    	 
    	 ///Click on link 
    	 driver.findElement(By.id("element_7")).click();
    	 String parent_window = driver.getWindowHandle();
    	 
    	 Set<String> window_name = driver.getWindowHandles();
    	 Iterator<String> iterator = window_name.iterator();
    	 while (iterator.hasNext()){
    	 subWindowHandler = iterator.next();
    	 driver.switchTo().window(subWindowHandler);
         String subwindow_title = driver.getTitle();
    	 System.out.println("current page Url is"+" "+subwindow_title);
    		}
    	 //to close current window 
    	  driver.close();
    	  driver.switchTo().window(parent_window); 

    	 
     }
	   @AfterClass
       public void teardown()
	    {
	     driver.close();
		}

}
