package com.pooja;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Automate_HomeAutomation_JUNIT {
	WebDriver driver;
	@BeforeClass
	public static void beforeClassTest(){
		System.out.println("i am here in beforeClassTest");
	}
	@Before
	public void beforeEveryTest(){
		System.out.println("i am here in beforeEveryTest");
	}
	@AfterClass
	public static void AfterClassTest(){
		System.out.println("i am here in AfterClassTest");
	}
	@After
	public void AfterEveryTest(){
		System.out.println("i am here in AfterEveryTest");
	}
	
	@Test
     public void test_homeautomation() throws InterruptedException {
		 System.out.println("Test case 1");
	 driver = new ChromeDriver();
	 driver.manage().window().maximize();
	 driver.get("http://localhost:8080/HomeAutomation/");
	 Thread.sleep(5000);
	 
	 Actions act = new Actions(driver);
	 WebElement first_name = driver.findElement(By.id("element_1_1"));
	 act.moveToElement(first_name).click();
	 act.sendKeys("pooja");
	 act.build().perform();
	 WebElement last_name = driver.findElement(By.id("element_1_2"));
	 act.moveToElement(last_name).click();
	 act.sendKeys("Phatak");
	 act.build().perform();
	 
	 
	 
	 
	 Thread.sleep(5000);
	 
	 WebElement submit = driver.findElement(By.id("saveForm"));
	 submit.click();
	
	 driver.close();
	}
	
	
	@Test
    public void testCase2() throws InterruptedException {
	
	 System.out.println("Test case 2");
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		Automate_HomeAutomation_JUNIT a=new Automate_HomeAutomation_JUNIT();
		a.test_homeautomation();
	}

}
