package com.pooja;

public class Ptestchild extends Ptest {
	
	public void displayDetails() { 
		count = 4;    // Line 1
		System.out.println(count + " "); 
	} 
	public static void main(String args[]) { 
		Ptest mySample = new Ptest(); 
		mySample.displayDetails(); 
		mySample = new Ptestchild();  
		mySample.displayDetails(); 
	}

}
